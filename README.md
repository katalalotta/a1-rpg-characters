# Java Assignment 1: RPG Characters

## Table of contents

- Background
- Installation
- Usage
- Contributors


### Background
Role playing games begin with a character creation. This project describes four different characters. All characters follow the well-known three-stat-system.

There are four character classes:

- Mage
- Ranger
- Rogue
- Warrior


All characters can equip items - weapons and armors. Each character is able to equip specified types of items. Items are equipped in different slots. Weapon can only be equipped to weapon slot, armors can be equipped to either head, body or legs.

In all, there are 7 types of weapons and 4 types of armors.

Weapon types are:

- Axe
- Bow
- Dagger
- Hammer
- Staff
- Sword
- Wand


Armor types are:

- Cloth
- Leather
- Mail
- Plate

### Installation

1. Clone this repository with HTTPS
2. Open project in IDE

### Usage

At the moment there is no game implementation. 
The only way to use characters is to run jUnit tests or test it with the Main-class.
In order to run the tests, right click the tests-folder and press *Run 'All Tests'*

### Contributors

This exercise was created by Noroff School of Technology and this solution was implemented by Lotta Lampola.






