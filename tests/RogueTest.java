import org.junit.jupiter.api.Test;
import rpg.characters.AbstractCharacter;
import rpg.characters.PrimaryAttributes;
import rpg.characters.Rogue;

import static org.junit.jupiter.api.Assertions.*;

class RogueTest {

    AbstractCharacter rogue = new Rogue();
    PrimaryAttributes bPA = rogue.getbPA();
    @Test
    public void levelUp_levelIncreased(){
        int oldLevel = rogue.getLevel();
        rogue.levelUp();
        assertEquals(1, rogue.getLevel()-oldLevel);
    }

    @Test
    public void levelUp_StrengthIncreased(){
        int oldS = bPA.getStrength();
        rogue.levelUp();
        assertEquals(1,bPA.getStrength()-oldS);
    }
    @Test
    public void levelUp_DexterityIncreased(){
        int oldS = bPA.getDexterity();
        rogue.levelUp();
        assertEquals(4,bPA.getDexterity()-oldS);
    }
    @Test
    public void levelUp_IntelligenceIncreased(){
        int oldS = bPA.getIntelligence();
        rogue.levelUp();
        assertEquals(1,bPA.getIntelligence()-oldS);
    }
}