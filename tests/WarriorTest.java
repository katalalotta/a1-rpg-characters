import org.junit.jupiter.api.Test;
import rpg.characters.AbstractCharacter;
import rpg.characters.PrimaryAttributes;
import rpg.characters.Warrior;

import static org.junit.jupiter.api.Assertions.*;

class WarriorTest {

    AbstractCharacter warrior = new Warrior();
    PrimaryAttributes bPA = warrior.getbPA();
    @Test
    public void levelUp_levelIncreased(){
        int oldLevel = warrior.getLevel();
        warrior.levelUp();
        assertEquals(1, warrior.getLevel()-oldLevel);
    }

    @Test
    public void levelUp_StrengthIncreased(){
        int oldS = bPA.getStrength();
        warrior.levelUp();
        assertEquals(3,bPA.getStrength()-oldS);
    }
    @Test
    public void levelUp_DexterityIncreased(){
        int oldS = bPA.getDexterity();
        warrior.levelUp();
        assertEquals(2,bPA.getDexterity()-oldS);
    }
    @Test
    public void levelUp_IntelligenceIncreased(){
        int oldS = bPA.getIntelligence();
        warrior.levelUp();
        assertEquals(1,bPA.getIntelligence()-oldS);
    }
}