import org.junit.jupiter.api.Test;
import rpg.characters.AbstractCharacter;
import rpg.characters.Rogue;
import rpg.characters.Warrior;
import rpg.exceptions.ArmorException;
import rpg.exceptions.WeaponException;
import rpg.items.Armor;
import rpg.items.Slot;
import rpg.items.Weapon;

import static org.junit.jupiter.api.Assertions.*;
import static org.junit.jupiter.api.Assertions.assertThrows;

class AbstractCharacterTest {

    Weapon testW = new Weapon(Weapon.WeaponType.AXE,7,1.1,1,Slot.WEAPON);
    Armor testA = new Armor(Armor.ArmorType.PLATE,1, Slot.BODY,1,0,0);

    AbstractCharacter warrior = new Warrior();

    @Test
    void levelIs1WhenCreated(){
        AbstractCharacter rogue = new Rogue();
        assertEquals(1,rogue.getLevel());
    }

    @Test
    void levelUp() {
        AbstractCharacter rogue = new Rogue();
        rogue.levelUp();
        assertEquals(2,rogue.getLevel());
    }

    @Test
    void levelNotHighEnoughForWeapon() throws WeaponException {
        AbstractCharacter warrior = new Warrior();
        Weapon testW = new Weapon(Weapon.WeaponType.AXE,7,1.1,2,Slot.WEAPON);
        //warrior.equipAWeapon(Slot.BODY,testW);
        String expected = "Warrior needs to be at least on level 2 in order to equip AXE";
        WeaponException thrown = assertThrows(
                WeaponException.class,
                () -> warrior.equipAWeapon(Slot.BODY,testW)
        );

        assertEquals(expected,thrown.getMessage());
    }

    @Test
    void levelNotHighEnoughForArmor() throws ArmorException {
        AbstractCharacter warrior = new Warrior();
        Armor testA = new Armor(Armor.ArmorType.PLATE,2,Slot.BODY,1,0,0);
        String expected = "Warrior needs to be at least on level 2 in order to equip PLATE";
        ArmorException thrown = assertThrows(
                ArmorException.class,
                () -> warrior.equipAnArmor(Slot.BODY,testA)
        );
        assertEquals(expected,thrown.getMessage());
    }

    @Test
    void wrongWeaponType() throws WeaponException {
        AbstractCharacter warrior = new Warrior();
        Weapon testw = new Weapon(Weapon.WeaponType.BOW, 7,1.1,2,Slot.WEAPON);
        String expected = "BOW type of weapon not excepted for Warrior";
        WeaponException thrown = assertThrows(
                WeaponException.class,
                () -> warrior.equipAWeapon(Slot.BODY,testw)
        );

        assertEquals(expected,thrown.getMessage());
    }

    @Test
    void wrongArmorType() throws ArmorException {
        AbstractCharacter warrior = new Warrior();
        Armor testA = new Armor(Armor.ArmorType.CLOTH,2,Slot.BODY,1,0,0);
        String expected = "CLOTH type of armor not excepted for Warrior";
        ArmorException thrown = assertThrows(
                ArmorException.class,
                () -> warrior.equipAnArmor(Slot.BODY,testA)
        );
        assertEquals(expected,thrown.getMessage());

    }

    @Test
    void validWeaponEquipmentReturnsTrue() throws WeaponException {
        AbstractCharacter warrior = new Warrior();
        Weapon testW = new Weapon(Weapon.WeaponType.AXE,7,1.1,2,Slot.WEAPON);
        warrior.levelUp();
        assertTrue(warrior.equipAWeapon(Slot.WEAPON,testW));

    }
    @Test
    void validArmorEquipmentReturnsTrue() throws ArmorException {
        AbstractCharacter warrior = new Warrior();
        Armor testA = new Armor(Armor.ArmorType.PLATE,2,Slot.BODY,1,0,0);
        warrior.levelUp();
        assertTrue(warrior.equipAnArmor(Slot.WEAPON,testA));

    }

    //Instructions say with no equipment, warriors DPS = 5, shouldn't it be 5+2+1?
    @Test
    void calculateDPSWithNoEquipment(){
        AbstractCharacter warrior = new Warrior();
        double expectedDPS = 1.0*(1+5.0/100);
        assertEquals(expectedDPS,warrior.getDps());

    }

    @Test
    void calculateDPSWithWeaponAxe() throws WeaponException {
        AbstractCharacter warrior = new Warrior();
        Weapon testW = new Weapon(Weapon.WeaponType.AXE,7,1.1,1,Slot.WEAPON);
        warrior.equipAWeapon(Slot.WEAPON, testW);
        double expectedCharacterDPS = 1.0*(1+5.0/100);
        double expectedDPS = 7*1.1*expectedCharacterDPS;
        assertEquals(expectedDPS,warrior.getDps());

    }

    @Test
    void calculateDPSWithWeaponAndArmor() throws WeaponException,ArmorException{
        AbstractCharacter warrior = new Warrior();
        Weapon testW = new Weapon(Weapon.WeaponType.AXE,7,1.1,1, Slot.WEAPON);
        Armor testA = new Armor(Armor.ArmorType.PLATE,1,Slot.BODY,1,0,0);
        warrior.equipAWeapon(Slot.WEAPON,testW);
        warrior.equipAnArmor(Slot.BODY,testA);
        double expectedCharacterDPS = 1.0+6.0/100;
        double expectedDPS = 7*1.1*expectedCharacterDPS;
        assertEquals(expectedDPS,warrior.getDps());
    }
}