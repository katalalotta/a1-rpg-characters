import org.junit.jupiter.api.Test;
import rpg.characters.AbstractCharacter;
import rpg.characters.Mage;
import rpg.characters.PrimaryAttributes;

import static org.junit.jupiter.api.Assertions.*;

class MageTest {
    AbstractCharacter mage = new Mage();
    PrimaryAttributes bPA = mage.getbPA();

    @Test
    public void levelUp_levelIncreased(){
        int oldLevel = mage.getLevel();
        mage.levelUp();
        assertEquals(1, mage.getLevel()-oldLevel);
    }

    @Test
    public void levelUp_StrengthIncreased(){
        int oldS = bPA.getStrength();
        mage.levelUp();
        assertEquals(1,bPA.getStrength()-oldS);
    }
    @Test
    public void levelUp_DexterityIncreased(){
        int oldS = bPA.getDexterity();
        mage.levelUp();
        assertEquals(1,bPA.getDexterity()-oldS);
    }
    @Test
    public void levelUp_IntelligenceIncreased(){
        int oldS = bPA.getIntelligence();
        mage.levelUp();
        assertEquals(5,bPA.getIntelligence()-oldS);
    }
}