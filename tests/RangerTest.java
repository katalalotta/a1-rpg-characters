import org.junit.jupiter.api.Test;
import rpg.characters.AbstractCharacter;
import rpg.characters.PrimaryAttributes;
import rpg.characters.Ranger;

import static org.junit.jupiter.api.Assertions.*;

class RangerTest {

    AbstractCharacter ranger = new Ranger();
    PrimaryAttributes bPA = ranger.getbPA();

    @Test
    public void levelUp_levelIncreased(){
        int oldLevel = ranger.getLevel();
        ranger.levelUp();
        assertEquals(1, ranger.getLevel()-oldLevel);
    }

    @Test
    public void levelUp_StrengthIncreased(){
        int oldS = bPA.getStrength();
        ranger.levelUp();
        assertEquals(1,bPA.getStrength()-oldS);
    }
    @Test
    public void levelUp_DexterityIncreased(){
        int oldS = bPA.getDexterity();
        ranger.levelUp();
        assertEquals(5,bPA.getDexterity()-oldS);
    }
    @Test
    public void levelUp_IntelligenceIncreased(){
        int oldS = bPA.getIntelligence();
        ranger.levelUp();
        assertEquals(1,bPA.getIntelligence()-oldS);
    }
}