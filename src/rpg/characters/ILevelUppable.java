package rpg.characters;

import rpg.exceptions.ArmorException;
import rpg.exceptions.WeaponException;
import rpg.items.Armor;
import rpg.items.Slot;
import rpg.items.Weapon;

/**
 * Interface which describes the rules for character behaviour for outside of the character classes.
 */
public interface ILevelUppable {

    public void levelUp();

    public void updateBasePrimaryAttributes(int s, int d, int i);

    public boolean equipAWeapon(Slot slot, Weapon w) throws WeaponException;

    public boolean equipAnArmor(Slot slot, Armor a) throws ArmorException;
}
