package rpg.characters;

import rpg.items.Armor;
import rpg.items.Weapon;

import java.util.ArrayList;
import java.util.List;
/**
 * Represents a game character of type Rogue
 */
public class Rogue extends AbstractCharacter{

    /**
     * List of the weapons a Rogue character is allowed to equip
     */
    private List<Weapon.WeaponType> weaponsAllowed;
    /**
     * List of the armors a Rogue character is allowed to equip
     */
    private List<Armor.ArmorType> armorsAllowed;
    /**
     * Creates a Rogue-object using its superclass AbstractCharacter.
     * Constructor also initializes a list and stores the weapon and armor types allowed to the list.
     */
    public Rogue() {
        super("Rogue",2,6,1,2);
        weaponsAllowed = new ArrayList<>();
        weaponsAllowed.add(Weapon.WeaponType.DAGGER);
        weaponsAllowed.add(Weapon.WeaponType.SWORD);
        super.addWeaponsAllowed(weaponsAllowed);
        armorsAllowed = new ArrayList<>();
        armorsAllowed.add(Armor.ArmorType.LEATHER);
        armorsAllowed.add(Armor.ArmorType.MAIL);
        super.addArmorsAllowed(armorsAllowed);
    }
    /**
     * Increases the characters level by one and updates its primary attributes accordingly
     */
    @Override
    public void levelUp() {
        System.out.println("Rogue leveling up....");
        this.level+=1;
        this.bPA.increasePrimaryAttributes(1,4,1);
    }
}
