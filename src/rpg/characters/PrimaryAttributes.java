package rpg.characters;

public class PrimaryAttributes {
    /**
     * Strength value
     */
    private int strength;
    /**
     * Dexterity value
     */
    private int dexterity;
    /**
     * Intelligence value
     */
    private int intelligence;

    /**
     * Ordinal number of the objects (which has primary attributes) main primary attribute
     */
    private int mainPrimaryAttributeNo;


    /**
     * Creates an instance of a Primary Attribute object and sets the initial values of attributes
     * @param s strength
     * @param d dexterity
     * @param i intelligence
     * @param mPANo ordinal of main primary attribute
     */
    public PrimaryAttributes(int s, int d, int i, int mPANo) {
        increasePrimaryAttributes(s,d,i);
        this.mainPrimaryAttributeNo = mPANo;
    }

    /**
     * Increases the values of attributes by the values it is given in parametes.
     * @param s strength
     * @param d dexterity
     * @param i intelligence
     */
    public void increasePrimaryAttributes(int s, int d, int i){
        this.strength += s;
        this.dexterity += d;
        this.intelligence += i;
    }

    public int getStrength() {
        return strength;
    }

    public void setStrength(int strength) {
        this.strength = strength;
    }

    public int getDexterity() {
        return dexterity;
    }

    public void setDexterity(int dexterity) {
        this.dexterity = dexterity;
    }

    public int getIntelligence() {
        return intelligence;
    }

    public void setIntelligence(int intelligence) {
        this.intelligence = intelligence;
    }

    public int getMainPrimaryAttribute() {
        return mainPrimaryAttributeNo;
    }
}
