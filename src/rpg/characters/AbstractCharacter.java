package rpg.characters;

import rpg.exceptions.ArmorException;
import rpg.exceptions.WeaponException;
import rpg.items.Armor;
import rpg.items.Slot;
import rpg.items.Weapon;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Set;

/**
 * Abstract character is an abstract parent class of an implementable character class in a game.
 */
public abstract class AbstractCharacter implements ILevelUppable {
    /**
     * Characters name
     */
    String name;
    /**
     * Level of character which can be increased. Level of a character affects its properties.
     */
    int level;
    /**
     * The initial properties of a character
     */
    PrimaryAttributes bPA;
    /**
     * The total properties of a character, taking into account the characters possible armors equipped which increases its properties
     */
    PrimaryAttributes totalAttributes;
    /**
     * Characters all totalAttributes additioned with one another
     */
    int sumOfTotalAttributes;

    /**
     * List of the weapons a character can equip
     */
    List<Weapon.WeaponType> weaponsAllowed;
    /**
     * List of the armors a character can equip
     */
    List<Armor.ArmorType> armorsAllowed;

    /**
     * A hashmap of weapons equipped
     */
    HashMap<Slot, Weapon> weapons;
    /**
     * A hashmap of armors equipped
     */
    HashMap<Slot,Armor> armors;

    /**
     * Each character has a main primary attribute (strenght, dexterity or intelligence)
     * The total of main primary attributes = characters own main primary attribute + possible armors primary attribute.
     */
    private int totalMainPrimaryAttribute;

    /**
     * Characters Damage per Second. DPS = weaponDPS * (1+TotalMainPrimaryAttribute/100).
     * If a character has no weapons, weaponDPS=1.
     */
    private double dps;

    /**
     * Creates an instance of a character and stores its primary attributes to an object.
     * Creates another instance of Primary Attributes-class for storing the total attributes (taking possible armor account).
     * @param name
     * @param s for strength
     * @param d for dexterity
     * @param i for intelligence
     * @param mPANo represents an ordinal (1-3) for the characters main primary attribute.
     *              1=strength, 2=dexterity, 3=intelligence
     *              I.e. if mPANo=1, main primary attribute is strength.
     */
    public AbstractCharacter(String name, int s, int d, int i, int mPANo){
        this.name = name;
        this.level = 1;
        this.bPA = new PrimaryAttributes(s,d,i,mPANo);
        this.weapons = new HashMap<>();
        this.armors = new HashMap<>();
        this.weaponsAllowed = new ArrayList<>();
        this.armorsAllowed = new ArrayList<>();
        this.dps = 1;
        this.totalAttributes = new PrimaryAttributes(bPA.getStrength(), bPA.getDexterity(), bPA.getIntelligence(), bPA.getMainPrimaryAttribute());
    }


    public void addWeaponsAllowed(List<Weapon.WeaponType> wa){
        this.weaponsAllowed = wa;
    }

    public void addArmorsAllowed(List<Armor.ArmorType> wa){
        this.armorsAllowed = wa;
    }

    public int getLevel(){
        return this.level;
    }

    public PrimaryAttributes getbPA(){
        return this.bPA;
    }

    @Override
    public void levelUp(){
        this.level+=1;

    }


    @Override
    public void updateBasePrimaryAttributes(int s, int d, int i){
        this.bPA.increasePrimaryAttributes(s,d,i);
    }

    /**
     * Calls a method which checks if the weapon to be equipped is suitable for a specific character or not.
     * If validation passes, the method adds the weapon to the hashmap for storing and returns true.
     * @param slot slot where the character equips the weapon.
     * @param w weapon which is to be equipped.
     * @return true if weapon check is passed and the weapon is equipped
     * @throws WeaponException is thrown in the weaponCheck-method if the validation does not pass
     */
    @Override
    public boolean equipAWeapon(Slot slot, Weapon w) throws WeaponException {
        if(weaponCheck(w)) {
            weapons.put(slot,w);
            return true;
        }
            return false;
    }

    /**
     * Validates, if the weapon to be equipped is suitable for specific character or not.
     * First checks, if the weapon type is allowed.
     * Secondly checks, if the characters level is high enough for equipping the weapon.
     * @param w weapon to be equipped
     * @return true, if weapon type is allowed and the required level for equipping is not too high for the character.
     * @throws WeaponException if weapon type is not allowed or the characters level is too low for equipping.
     */
    public boolean weaponCheck(Weapon w) throws WeaponException {
        if (!weaponsAllowed.contains(w.getWeaponType())){
            throw new WeaponException(w.getWeaponType().name()+" type of weapon not excepted for "+this.name);
        }
        if (this.level >= w.getReqLevel()){
            return true;
        } else {
            throw new WeaponException(name+ " needs to be at least on level "+w.getReqLevel()+" in order to equip "+w.getWeaponType().name());
        }
    }

    /**
     * Calls a method which checks if the armor to be equipped is suitable for a specific character or not.
     * If validation passes, the method adds the armor to the hashmap for storing and returns true.
     * @param slot slot where the character equips the armor.
     * @param armor armor which is to be equipped.
     * @return true if check is passed and the armor is equipped
     * @throws ArmorException is thrown in the armorCheck-method if the validation does not pass
     */
    @Override
    public boolean equipAnArmor(Slot slot, Armor armor) throws ArmorException{
        if(ArmorCheck(armor)) {
            armors.put(slot,armor);
            totalAttributes.increasePrimaryAttributes(armor.getPa().getStrength(),armor.getPa().getDexterity(),armor.getPa().getIntelligence());
            return true;
        }
            return false;
    }

    /**
     * Validates, if the armor to be equipped is suitable for specific character or not.
     * First checks, if the armor type is allowed.
     * Secondly checks, if the characters level is high enough for equipping the armor.
     * @param a armor to be equipped
     * @return true, if armor type is allowed and the required level for equipping is not too high for the character.
     * @throws ArmorException if armor type is not allowed or the characters level is too low for equipping.
     */
    public boolean ArmorCheck(Armor a) throws ArmorException {
        if (!armorsAllowed.contains(a.getArmorType())) {
            throw new ArmorException(a.getArmorType().name()+" type of armor not excepted for "+this.name);
        }
        if (this.level >= a.getReqLevel()){
            return true;
        } else {
            throw new ArmorException(name+ " needs to be at least on level "+a.getReqLevel()+" in order to equip "+a.getArmorType().name());
        }

    }

    /**
     * Prints out all of the main statistics of a game character.
     */
    public void printStats(){
        System.out.println(name+" statistics:");
        System.out.println("* level: "+level);
        System.out.println("* strength: "+ totalAttributes.getStrength());
        System.out.println("* dexterity: "+ totalAttributes.getDexterity());
        System.out.println("* intelligence: "+ totalAttributes.getIntelligence());
        System.out.println("* DPS: "+dps);
    }


    /**
     * Calculates the characters DPS-value and returns it.
     * @return characters DPS
     */
    public double getDps() {
        Set<Slot> slotSet2 = weapons.keySet();
        Weapon w = null;
        for (Slot s: slotSet2){
            w = weapons.get(s);
        }
        if (weapons.isEmpty()) {
            this.dps = 1.0 * (1 +getTotalMainPrimaryAttribute()/100.0);;
        } else {
            this.dps = w.getDPS()*(1 + getTotalMainPrimaryAttribute()/100.0);
        }
        return dps;
    }

    public PrimaryAttributes getTotalAttributes() {
        return totalAttributes;
    }

    public int getSumOfTotalAttributes() {
        sumOfTotalAttributes = totalAttributes.getStrength()+ totalAttributes.getDexterity()+ totalAttributes.getIntelligence();
        return sumOfTotalAttributes;
    }

    /**
     * Checks which is the characters main primary attribute and returns its total value (taking armor into account)
     * @return total of main primary attribute
     */
    public int getTotalMainPrimaryAttribute() {
        int mainAttribute = this.getbPA().getMainPrimaryAttribute();
        if (mainAttribute == 1){
            totalMainPrimaryAttribute = totalAttributes.getStrength();
        } else if (mainAttribute == 2) {
            totalMainPrimaryAttribute = totalAttributes.getDexterity();
        } else {
            totalMainPrimaryAttribute = totalAttributes.getIntelligence();
        }
        return totalMainPrimaryAttribute;
    }
}

