package rpg.characters;

import rpg.items.Armor;
import rpg.items.Weapon;

import java.util.ArrayList;
import java.util.List;
/**
 * Represents a game character of type Warrior
 */
public class Warrior extends AbstractCharacter {
    /**
     * List of the weapons a Warrior character is allowed to equip
     */
    private List<Weapon.WeaponType> weaponsAllowed;
    /**
     * List of the armors a Warrior character is allowed to equip
     */
    private List<Armor.ArmorType> armorsAllowed;
    /**
     * Creates a Warrior-object using its superclass AbstractCharacter.
     * Constructor also initializes a list and stores the weapon and armor types allowed to the list.
     */
    public Warrior() {
        super("Warrior",5,2,1,1);
        weaponsAllowed = new ArrayList<>();
        weaponsAllowed.add(Weapon.WeaponType.AXE);
        weaponsAllowed.add(Weapon.WeaponType.SWORD);
        weaponsAllowed.add(Weapon.WeaponType.HAMMER);
        super.addWeaponsAllowed(weaponsAllowed);
        armorsAllowed = new ArrayList<>();
        armorsAllowed.add(Armor.ArmorType.MAIL);
        armorsAllowed.add(Armor.ArmorType.PLATE);
        super.addArmorsAllowed(armorsAllowed);
    }

    /**
     * Increases the characters level by one and updates its primary attributes accordingly
     */
    @Override
    public void levelUp() {
        System.out.println("Rogue leveling up....");
        this.level+=1;
        this.bPA.increasePrimaryAttributes(3,2,1);

    }

}
