package rpg.characters;

import rpg.items.Armor;
import rpg.items.Weapon;

import java.util.ArrayList;
import java.util.List;

/**
 * Represents a game character of type Mage
 */
public class Mage extends AbstractCharacter {

    /**
     * List of the weapons a Mage character is allowed to equip
     */
    private List<Weapon.WeaponType> weaponsAllowed;
    /**
     * List of the armors a Mage character is allowed to equip
     */
    private List<Armor.ArmorType> armorsAllowed;

    /**
     * Creates a Mage-object using its superclass AbstractCharacter.
     * Constructor also initializes a list and stores the weapon and armor types allowed to the list.
     */
    public Mage() {
        super("Mage",1,1,8,3);
        weaponsAllowed = new ArrayList<>();
        weaponsAllowed.add(Weapon.WeaponType.STAFF);
        weaponsAllowed.add(Weapon.WeaponType.WAND);
        super.addWeaponsAllowed(weaponsAllowed);
        armorsAllowed = new ArrayList<>();
        armorsAllowed.add(Armor.ArmorType.CLOTH);
        super.addArmorsAllowed(armorsAllowed);
    }


    /**
     * Increases the characters level by one and updates its primary attributes accordingly
     */
    @Override
    public void levelUp() {
        System.out.println("Mage leveling up....");
        this.level+=1;
        super.updateBasePrimaryAttributes(1,1,5);
    }

}
