package rpg.items;

public class Weapon extends AbstractItem{
    //Same attributes as all items
    private String name;
    private int reqLevel;
    private Slot slot;

    //rpg.items.Weapon spesific attributes

    public enum WeaponType {AXE,BOW,DAGGER,HAMMER,STAFF,SWORD,WAND}

    private WeaponType weaponType;

    private int damage;

    private double DPS;


    public Weapon(WeaponType wp, int damage, double attackSPeed, int reqLevel, Slot slot) {
        this.weaponType = wp;
        this.damage = damage;
        this.DPS = damage*attackSPeed;
        this.name = wp.name();
        this.reqLevel = reqLevel;
        this.slot = slot;
    }

    public WeaponType getWeaponType() {
        return weaponType;
    }

    public int getReqLevel() {
        return reqLevel;
    }

    public double getDPS(){
        return this.DPS;
    }
}
