package rpg.items;

/**
 * Abstract item is an abstract parent class for weapons and armors.
 * In this implementation I failed to use it.
 */
public abstract class AbstractItem {
    //abstract class which is inherited by weapon and armor
    String name;
    int reqLevel;
    Slot slot;
}
