package rpg.items;

import rpg.characters.PrimaryAttributes;

/**
 * Armor is an equipment for a character to have. It increases characters primary attributes (strength, dexterity and intelligence).
 */
public class Armor extends AbstractItem{
    //item related attributes
    /**
     * Armors name
     */
    private String name;
    /**
     * Level required in order to equip the armor
     */
    private int reqLevel;
    /**
     * Slot where to have the armor (BODY,LEG,HEAD)
     */
    private Slot slot;

    //Armor spesific attributes

    /**
     * All armor types as enumeration
     */
    public enum ArmorType {CLOTH,LEATHER,MAIL,PLATE}

    /**
     * Armor type of instance
     */
    private ArmorType armorType;
    /**
     * Armor has primary attributes (strength, dexterity and intelligence) which are stored in a separate Primary Attribute-object.
     * Armor has no main primary attribute so its ordinal is set to zero.
     */
    private PrimaryAttributes pa;

    /**
     *
     * @param at armor type
     * @param reqLevel required level in order to equip
     * @param slot slot where to equip
     * @param s armor strength
     * @param d armor dexterity
     * @param i armor intelligence
     */
    public Armor(ArmorType at, int reqLevel, Slot slot, int s, int d, int i){
        this.armorType = at;
        this.reqLevel = reqLevel;
        this.slot = slot;
        this.name = at.name();
        this.pa = new PrimaryAttributes(s,d,i,0);
    }

    public int getReqLevel() {
        return reqLevel;
    }

    public ArmorType getArmorType() {
        return armorType;
    }

    public PrimaryAttributes getPa() {
        return pa;
    }
}
