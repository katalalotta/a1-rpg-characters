package rpg.items;

/**
 * Slot is an enumeration of slots where characters can store equipment (weapons and armors)
 */
public enum Slot {
    HEAD,BODY,LEGS,WEAPON
}
