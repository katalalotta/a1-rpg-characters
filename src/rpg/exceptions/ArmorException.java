package rpg.exceptions;

/**
 * Custom exception which is used with methods concerning equipping armors
 */
public class ArmorException extends Exception {
    public ArmorException(String message){
        super(message);
    }
}
