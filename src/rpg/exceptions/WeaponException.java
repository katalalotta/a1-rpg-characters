package rpg.exceptions;
/**
 * Custom exception which is used with methods concerning equipping weapons
 */
public class WeaponException extends Exception{

    public WeaponException(String message) {
        super(message);
    }
}
